<?php
	/* Modified version of import.config.js for drupal
	*/

	$chars = array(">", "<" , "||period||", "||protocol||");
	$repl = array("", "" , ".","http");

	$fc_base_root = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? 'https' : 'http';
    	$fc_base_url = $fc_base_root .= '://' . $_SERVER['HTTP_HOST'];
     	$fc_base_url .= '/' . trim(mb_substr($_SERVER['SCRIPT_NAME'],0,(strripos($_SERVER['SCRIPT_NAME'],'fcchat'))), '\,/');
?>
//Path to FCPlayer Folder (must end in a backslash)' .
var fc_chat_path = "<?php echo $fc_base_url; ?>/FCChat/";
var fc_style_template = "<?php echo str_replace($chars, $repl, $_GET['var1']); ?>.js";
if(!window["FCChatConfig"]){
	document.write("<script type='text/javascript' src='" + fc_chat_path + "config/config.js'></script>");
}