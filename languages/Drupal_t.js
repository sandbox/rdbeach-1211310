/*
 * LANGUAGE TEMPLATE
 *
 * FCChat supports any language which uses the Latin alphabet. It does not have full support
 * for other alphabets at this time. 
 * 
 * To use this template, edit FCChat/config/config.js as follows:
 * 
 *  //language
 *    language_template:"english.js",
 *    
 * READ THIS BEFORE EDITING THIS TEMPLATE:
 * 
 * In some instances,  space is limited, so be brief with your descriptions and/or use 
 * abbreviations (button text, for example). Some text options have a width parameter 
 * associated with them, which specifies the width, in pixels, of the corresponding 
 * text element. This is used to establish the correct spacing between elements.
 * 
 * In this file comments are preceded by double slashes ie.
 *      //THIS IS A COMMENT
 *      
 * Only edit text that appears between double quotes, ie.
 * 
 *      t1: "MAKE YOUR CHANGES HERE",
 *      
 * DO not change any other elements of this file.
 * 
 * Please note: administrative functions are still in English only.
 * 
 * If you create a decent translation to a new language. Please email us the template at 
 * support@fastcatsoftware.com
 */

FCChatConfig.txt={

	//guest login may contain letters,numbers and underscores only, 10 chars max
	t1: Drupal.t("guest"),
	t2: Drupal.t("Chat"),
	t3: Drupal.t("online"),
	
	//Buttons
	t4: Drupal.t("On"),
	t5: Drupal.t("Off"),
	t6: Drupal.t("Open Chat"),
	t7: Drupal.t("Help"),
	t8: Drupal.t("Close"),
	close_width: 51,
	t9: Drupal.t("Options"),
	options_width: 61,
	t10: Drupal.t("Send to Room"),
	t11: Drupal.t("Send Private"),
	send_buttons_width: 95,
	
	//Section Headings
	t12: Drupal.t("Chat Room"),
	chat_room_width: 61,
        t13: Drupal.t("Back to chat"),
	t14: Drupal.t("Private Chat"),
	private_chat_width: 68,
	t15: Drupal.t("Who's Online"),
	t16: Drupal.t("Chat Room Members"),
	t17: Drupal.t("Private Group"),
	
	//Tooltips
	t18: Drupal.t("Clear"),
	t19: Drupal.t("Room Alerts On"),
	t20: Drupal.t("Room Alerts Off"),
	t21: Drupal.t("Delete All"),
	t22: Drupal.t("Filter Conversations"),
	t23: Drupal.t("Bold Text"),
	t24: Drupal.t("Italic Text"),
	t25: Drupal.t("Underline Text"),
	t26: Drupal.t("Insert Image Tag"),
	t27: Drupal.t("Upload Images"),
	t28: Drupal.t("Upload Images Disabled"),
	t29: Drupal.t("Create Avatar"),
	t30: Drupal.t("Create Avatar Disabled"),
	t31: Drupal.t("Video Chat"),
	t32: Drupal.t("Video Chat Disabled"),
	t33: Drupal.t("Webcam"),
	t34: Drupal.t("Change Avatar"),
	
	//Textbox inserts
	t35: Drupal.t("Type Message Here!"),
	t36: Drupal.t("image name"),
	t37: Drupal.t("=b="),
	t38: Drupal.t("=i="),
	t39: Drupal.t("=u="),
	
	//Room Members Panel
	t40: Drupal.t("Room"),
	t41: Drupal.t("Change"),
	
	//User Dialog
	user_dialog_width: 340,
	t43: Drupal.t("Sign in"),
	t44: Drupal.t("Sign out"),
	t45: Drupal.t("Mod"),
	t46: Drupal.t("Profile"),
	t47: Drupal.t("Change Avatar"),
	t48: Drupal.t("Status"),
	t49: Drupal.t("Add to private group"),
	t50: Drupal.t("Block"),
	t51: Drupal.t("*Hey, it's me! "),
	t52: Drupal.t("*This user has blocked you."),
	t53: Drupal.t("*This user is offline."),
	t54: Drupal.t("Close"),
	t55: Drupal.t("Retrieving...(to private chat)"),
	
	//Sign in Dialog
	t56: Drupal.t("Name"),
	t57: Drupal.t("Password"),
	t58: Drupal.t("Enter"),
	t59: Drupal.t("Cancel"),
	t60: Drupal.t("Please enter your desired screen name and password."),
	t61: Drupal.t("Screen names and passwords must contain 3-15 characters each. Only letters, numbers, and underscores are allowed."),
	t62: Drupal.t("Screen names may not begin with 'guest'"),
	t63: Drupal.t("The screen name you entered is invalid. Please choose a different screen name."),
	t64: Drupal.t("The screen name you entered is already in use. Please provide the correct password or choose a different screen name."),
	
	//Options Dialog
	t65: Drupal.t("Room List"),
	t66: Drupal.t("Create New Room"),
	t67: Drupal.t("Display Timestamp:"),
	t68: Drupal.t("yes"),
	t69: Drupal.t("no"),
	t70: Drupal.t("Mode:"),
	t71: Drupal.t("window"),
	t72: Drupal.t("split-screen"),
	t73: Drupal.t("Sounds"),
	t74: Drupal.t("Sounds:"),
	t75: Drupal.t("on"),
	t76: Drupal.t("off"),
	t77: Drupal.t("Room Message Alert"),
	t78: Drupal.t("Private Message Alert"),
	t79: Drupal.t("Enter Room Alert"),
	t80: Drupal.t("Leaving Room Alert"),
	t81: Drupal.t("Font Size"),
	t82: Drupal.t("Font Color"),
	
	t255: Drupal.t("Translator"),
	t256: Drupal.t("Translate incoming chats to: "),
	
	//Language Names: must match language_codes in the current style
	t257: [Drupal.t("translator off"),Drupal.t("Arabic"),Drupal.t("Bulgarian"),Drupal.t("Catalan"),Drupal.t("Chinese Simplified"),Drupal.t("Chinese Traditional"),Drupal.t("Czech"),Drupal.t("Danish"),Drupal.t("Dutch"),Drupal.t("English"),Drupal.t("Estonian"),Drupal.t("Finnish"),Drupal.t("French"),Drupal.t("German"),Drupal.t("Greek"),Drupal.t("Haitian Creole"),Drupal.t("Hebrew"),Drupal.t("Hungarian"),Drupal.t("Indonesian"),Drupal.t("Italian"),Drupal.t("Japanese"),Drupal.t("Korean"),Drupal.t("Latvian"),Drupal.t("Lithuanian"),Drupal.t("Norwegian"),Drupal.t("Polish"),Drupal.t("Portuguese"),Drupal.t("Romanian"),Drupal.t("Russian"),Drupal.t("Slovak"),Drupal.t("Slovenian"),Drupal.t("Spanish"),Drupal.t("Swedish"),Drupal.t("Thai"),Drupal.t("Turkish"),Drupal.t("Ukrainian"),Drupal.t("Vietnamese")],
	
	//Color Names: must match color_values in the current style
	t83: [Drupal.t("default"),Drupal.t("black"),Drupal.t("blue"),Drupal.t("red"),Drupal.t("purple"),Drupal.t("green"),Drupal.t("yellow"),Drupal.t("orange"),Drupal.t("white")], 
	
	t84: Drupal.t("Block List"),
	t85: Drupal.t("(a/v)"),
	t86: Drupal.t("edit"),
	t87: Drupal.t("delete"),
	t88: Drupal.t("clear"),
	
	//Room Password Dialog
	t89: Drupal.t("This room is password protected."),
	t90: Drupal.t("Password"),
	t91: Drupal.t("Enter"),
	t92: Drupal.t("Cancel"),
	t93: Drupal.t("Room passwords must contain 3-15 characters each. Only letters, numbers, and underscores are allowed."),
	t94: Drupal.t("The password is incorrect. Please try again"),
	t95: Drupal.t("Sorry, the maximum number of rooms has been reached."),
	
	//Alert Dialog
	alert_dialog_width: 260,
	t96: Drupal.t("<font style='color:red;font-weight:700'>Connection lost...</font> <a href='javascript:fc_chat.reqRecon()'>Reconnect</a> <a href='javascript:fc_chat.closeChat()'>Close</a>"),
	t97: Drupal.t("Signing Off..."),
	t98: Drupal.t("<font style='color:red;font-weight:700'>Sorry, the room has closed...</font><br><center><a href='javascript:fc_chat.reqRecon()'>Reconnect</a> <a href='javascript:fc_chat.closeChat()'>Close</a></center>"),
	t99: Drupal.t("To enter, please <b>Login</b> first. <a href='javascript:fc_chat.closeChat()'>Close</a>"),
	
	//Widget Msgs
	t100: Drupal.t("<b>Connecting, please wait...</b>"),
	t101: Drupal.t("You have successfully logged out"),
	t102: Drupal.t("<b>&nbsp;Chat is off.&nbsp;&nbsp;</b>"),
	t103: Drupal.t("<b>Chat Paused... <a href='javascript:fc_chat.unPause()'>Resume</a></b>"),
	t104: Drupal.t("<font style='color:red;font-weight:700'>Connection lost...</font> <a href='javascript:fc_chat.reqRecon()'>Reconnect</a>"),
	t105: Drupal.t("Connection failed. Trying again. Attempt"),
	t106: Drupal.t("Sorry, could not connect. Giving up."),
	t254: Drupal.t("New message...Roll over to expand."),
	
	//Chat Rooms
	t107: Drupal.t("Welcome!!! Your screen name is"),
	t108: Drupal.t("To sign in, click on your screen name in the side bar."),
	t109: Drupal.t("You are currently chatting in: "),
	t110: Drupal.t("<a href='javascript:void' onClick='fc_chat.show_hide_options(333,425);return false'>See Room List</a>"),
	t111: Drupal.t("Prev"),
	t112: Drupal.t("Previous"),
	t113: Drupal.t("Next"),
	t114: Drupal.t("SYSTEM"),
	t115: Drupal.t("to"),
	t116: Drupal.t("Archive Page"),
	t117: Drupal.t("Current Chat"),
	t118: Drupal.t("Autoclear. Room message limit exceded."),
	t119: Drupal.t("**Comment Erased**"),
	t120: Drupal.t("<b><font style='margin-left:10px;font-size:12pt;'>&nbsp;Loading Chat...Please wait...</font></b>"),
	t121: Drupal.t("<b><font style='margin-left:10px;font-size:12pt;'>&nbsp;Loading Page...Please wait...</font></b>"),
	t122: Drupal.t("<b><font style='margin-left:10px;font-size:12pt;'>&nbsp;Loading Room...Please wait...</font></b>"),
	
	//Account types
	t123: Drupal.t("mod"),
	t124: Drupal.t("admin"),
	
	//user state
	t125: Drupal.t("typing"),
	t126: Drupal.t("idle"),
	
	//Room Alerts
	t127: Drupal.t(" has left the room."),
	t128: Drupal.t(" has entered the room."),
	t129: Drupal.t("You must be a member to enter this room."),
	
	//Send chat msgs
	t130: Drupal.t("Unable to send this message. The user is offline."),
	t131: Drupal.t("Message is too big ("),
	t132: Drupal.t(" chars max)."),
	t133: Drupal.t("Please enter a message!"),
	
	//Split screen mode
	t134: Drupal.t("Split-screen mode is not currently enabled."),
	
	//Remove from block list
	t135: Drupal.t("Remove"),
		
	//Dates and times
	
	//before minutes (singular/plural)
	t136: Drupal.t(""),
	t137: Drupal.t(""),
	//after minutes (singular/plural)
	t138: Drupal.t(" minute ago"),
	t139: Drupal.t(" minutes ago"),
	//before hours (singular/plural)
	t140: Drupal.t(""),
	t141: Drupal.t(""),
	//after hours (singular/plural)
	t142: Drupal.t(" hour ago"),
	t143: Drupal.t(" hours ago"),
	//before days (singular/plural)
	t144: Drupal.t(""),
	t145: Drupal.t(""),
	//after days (singular/plural)
	t146: Drupal.t(" day ago"),
	t147: Drupal.t(" days ago"),
	t148: Drupal.t("just now"),
	t149: Drupal.t("AM"),
	t150: Drupal.t("PM"),
	
	//Application Windows
	t151: Drupal.t("Chat Window"),
	t152: Drupal.t("FCChat Video"),
	t153: Drupal.t("FCChat Video"),
	t154: Drupal.t("minimize"),
	t155: Drupal.t("maximize"),
	t156: Drupal.t("restore"),
	t157: Drupal.t("close"),
	
	//Other Status Msgs
	t158: Drupal.t("Please wait"),
	t159: Drupal.t("<b>Connecting, please wait...</b>"),
	t160: Drupal.t("Not Ready... You may use the chat-box, located in the sidebar, to enter the chat rooms."),
	t161: Drupal.t("Sorry...An Error Occurred. Please Refresh"),
	
	//Upload page
	t162: Drupal.t("Share Images"),
	t163: Drupal.t("Invalid User! Please Try again."),
	t164: Drupal.t("Image Name may not contain [[ or ]]. Please Try again."),
	t165: Drupal.t("Bad Filetype! Please Try again."),
	t166: Drupal.t("You have exceeded the size limit for image files! Please Try again."),
	t167: Drupal.t("The image file repository is full! Please Try again."),
	t168: Drupal.t("Please enter a valid file."),
	t169: Drupal.t("Upload Unsuccessful! Try again"),
	t170: Drupal.t("No image selected. Please Try again."),
	t171: Drupal.t("Uploaded Successfully!"),
	t172: Drupal.t("Step 1:"),
	t173: Drupal.t("Step 2:"),
	t174: Drupal.t("In order to use this image in your chat messages, simply copy and paste the following..."),
	t175: Drupal.t("...into the chat box below."),
	t176: Drupal.t(" Upload the image that you would like to use in your chat messages."),
	t177: Drupal.t("Upload Image"),
	t178: Drupal.t("(jpg, gif,and png only. Maximum size:"),
	t179: Drupal.t("Please Note:"),
	t180: Drupal.t("You may include a maximum of three images in any single chat message."),
	t181: Drupal.t("Back"),
	t182: Drupal.t("Finish"),
	
	//Avatar page
	t183: Drupal.t("Upload Avatar"),
	t184: Drupal.t("Select Avatar"),
	t185: Drupal.t("Please Wait..."),
	t186: Drupal.t("You have successfully uploaded a new avatar!"),
	t187: Drupal.t("The width and height of the images can be no larger than "),
	t188: Drupal.t("px and"),
	t189: Drupal.t("px, respectively. Please try again."),
	t190: Drupal.t("Option One:"),
	t191: Drupal.t("Option Two:"),
	t192: Drupal.t("Option Three:"),
	t193: Drupal.t("Option Four:"),
	t194: Drupal.t("Upload a new avatar. The maximum width and height for avatars is"),
	t195: Drupal.t("px"),
	t196: Drupal.t("Use your"),
	t197: Drupal.t("avatar"),
	t198: Drupal.t("Submit"),
	t199: Drupal.t("Link to gravatar Image.<br>(ie http://www.gravatar.com/avatar/1234.png)"),
	t200: Drupal.t("Use your current forum avatar"),
	t201: Drupal.t("Use my current avatar"),
	t202: Drupal.t("Select an avatar from the gallery below"),
	t203: Drupal.t("You have"),
	t204: Drupal.t("successfully selected an avatar from the gallery!"),
	t205: Drupal.t("elected to use your current forum avatar!"),
	t206: Drupal.t("elected to use your Gravatar avatar!"),
	
	//Profile page
	t207: Drupal.t("User Profile"),
	t208: Drupal.t("Loading..."),
	t209: Drupal.t("Sorry, no profile yet."),
	t210: Drupal.t("Sorry, profiles are not available for guest accounts"),
	t211: Drupal.t("Please sign in to create a profile"),
	t212: Drupal.t("Name"),
	t213: Drupal.t("Age"),
	t214: Drupal.t("Male"),
	t215: Drupal.t("Female"),
	t216: Drupal.t("Gender"),
	t217: Drupal.t("Location"),
	t218: Drupal.t("About Me"),
	t219: Drupal.t("chars"),
	t220: Drupal.t("Save Profile"),
	t221: Drupal.t("Saving..."),
	t222: Drupal.t("Saved"),
	t223: Drupal.t("Return to chat"),
		
	//Video
	t224: Drupal.t("Video Chat Loading...This may take a few seconds."),
	t225: Drupal.t("Sorry, you may not open the video chat from this room. To use the video chat, you must be in a room which has (a/v) next to it."),
	t226: Drupal.t("This user is chatting in the private room: "),
	t227: Drupal.t(". You must be a member of this room in order to view their webcam."),
	t228: Drupal.t("Sorry, the video chat has reached its capacity. Try again later."),
	t229: Drupal.t("Turn on camera"),
	t230: Drupal.t("Turn off camera"),
	t231: Drupal.t("Broadcast audio"),
	t232: Drupal.t("Mute"),
	t233: Drupal.t("Loading"),
	t234: Drupal.t("No Cam Found"),
	t235: Drupal.t("Start Your Cam"),
	t236: Drupal.t("Waiting for Signal"),
	t237: Drupal.t("Connection Lost"),
	t238: Drupal.t("Server Busy"),
	t239: Drupal.t("Expand"),
	
	//Administration
	t240: Drupal.t("Censor"),
	//before name
	t241: Drupal.t("Status Report for"),
	//after name
	t242: Drupal.t(":"),
	t243: Drupal.t("This user has been blocked by the administrator from chatting in all rooms."),
	t244: Drupal.t("This user been blocked by the administrator from using the private chat."),
	t245: Drupal.t("This user has been blocked from sending private messages for approx."),
	t246: Drupal.t("mins."),
	t247: Drupal.t("This user has been blocked by the administrator from chatting in the current room."),
	t248: Drupal.t("This user is in good standing."),
	t249: Drupal.t("Current room: none."),
	t250: Drupal.t("Current room:"),
	t251: Drupal.t("Time Online:"),
	t252: Drupal.t("Idle time: less than one minute."),
	t253: Drupal.t("Idle time:")

};