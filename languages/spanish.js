/*
 * Plantillas de Lenguaje
 *
 * ADVERTENCIA: Este es una traducci�n autom�tica!
 *
 * FCChat es compatible con cualquier idioma que utilice el alfabeto latino. No tiene apoyo
 * de otros alfabetos en este momento.
 *
 * Para utilizar esta plantilla, editar FCChat/config/config.js  de la siguiente manera:
 *
 *  //Idioma
 *    language_template: "spanish.js",
 *
 * LEA ESTO ANTES DE EDICI�N DE ESTE MODELO:
 *
 * En algunos casos, el espacio es limitado, por lo que ser breve con sus descripciones y/o uso
 * Abreviaturas (texto del bot�n, por ejemplo). Algunas opciones de texto tienen un par�metro de ancho
 * Asociados con ellos, que especifica la anchura, en p�xeles, de la correspondiente
 * Elemento de texto. Esto se utiliza para establecer la separaci�n correcta entre los elementos.
 *
 * En este archivo comentarios est�n precedidos por dos barras, es decir.
 *      //Esto es un comentario
 *
 * S�lo modificar el texto que aparece entre comillas dobles, es decir.
 *
 * T1: "Realice los cambios aqu�",
 * 
 * No cambie ning�n otro elemento de este archivo.
 *
 * Nota: las funciones administrativas todav�a est�n en Ingl�s solamente.
 *
 * Si se crea una traducci�n decente a un nuevo idioma. Por favor, env�enos un correo electr�nico a la plantilla
 * support@fastcatsoftware.com
 */

FCChatConfig.txt = {

	// Acceso para invitados pueden contener letras, n�meros y subrayados, de 10 caracteres m�ximo
	t1: "invitado",
	t2: "Chat",
	t3: "en l�nea",
	
	// Botones
	t4: "Encender",
	t5: "Apagar",
	t6: "Abrir Chat",
	t7: "Ayuda",
	t8: "Cerrar",
	close_width: 54,
	t9: "Opciones",
	options_width: 70,
	t10: "Enviar a la Sala",
	t11: "Enviar privado",
	send_buttons_width: 95,
	
	// Secci�n de T�tulos
	t12: "Sala de Chat",
	chat_room_width: 72,
	t13: "Volver al Chat",
	t14: "Chat Privado",
	private_chat_width: 72,
	t15: "Qui�n est� en l�nea",
	t16: "Miembros de la Sala",
	t17: "Grupo Privado",
	
	// Informaci�n sobre herramientas
	t18: "Borrar",
	t19: "Alertas de sala.",
	t20: "Alertas de sala.",
	t21: "Eliminar todas",
	t22: "Filtrar las conversaciones",
	t23: "Texto en negrita",
	t24: "Texto en cursiva",
	t25: "Texto subrayado",
	t26: "Insertar im�gen",
	t27: "Subir Im�genes ",
	t28: "Subir Im�genes se ha desactivado",
	t29: "Crear Avatar",
	t30: "Crear Avatar se ha desactivado",
	t31: "Video Chat",
	t32: "Video Chat se ha desactivado",
	t33: "Webcam",
	t34: "Cambiar Avatar",
	
	// Inserta cuadro de texto
	t35: "Escriba mensaje aqu�!",
	t36: "nombre de la im�gen",
	t37: "=b=",
	t38: "=i=",
	t39: "=u=",
	
	// Los miembros del panel de habitaciones
	t40: "Sala",
	t41: "Cambiar",
	
	// Usuario de di�logo
	user_dialog_width: 340,
	t43: "Iniciar sesi�n",
	t44: "Cerrar sesi�n",
	t45: "Mod",
	t46: "Perfil",
	t47: "Cambiar Avatar",
	t48: "Estado",
	t49: "Agregar a grupo privado",
	t50: "Bloquear",
	t51: "* Oye, soy yo",
	t52: "* Este usuario ha bloqueado",
	t53: "* Este usuario est� fuera de l�nea.",
	t54: "Cerrar",
	t55: "Recuperaci�n de ... (para chat privado)",
	
	// Firma en el di�logo
	t56: "Nombre",
	t57: "Contrase�a",
	t58: "Entrar",
	t59: "Cancelar",
	t60: "Ingrese su nombre de usuario y contrase�a deseados, por favor.",
	t61: "Los nombres de usuario y las contrase�as deben contener 3-15 caracteres cada uno. S�lo letras, n�meros y guiones est�n permitidos.",
	t62: "El nombre de usuario no puede comenzar con 'invitado'.",
	t63: "El nombre de usuario introducido no es v�lido. Elija un nombre de usuario diferente, por favor.",
	t64: "El nombre de usuario que ha introducido ya est� en uso. Introduzca la contrase�a correcta o seleccione un nombre de usuario diferente, por favor.",
	
	// De di�logo Opciones
	t65: "Lista de Salas",
	t66: "Crear una nueva sala",
	t67: "Marca de tiempo",
	t68: "s�",
	t69: "no",
	t70: "Modo",
	t71: "ventana",
	t72: "pantalla dividida",
	t73: "Sonidos",
	t74: "Sonidos",
	t75: "encender",
	t76: "apagar",
	t77: "Alerta para mens�jes de sala",
	t78: "Alerta para mens�jes privados",
	t79: "Alerta para entrar sala",
	t80: "Alerta para dejar sala",
	t81: "Tama�o de fuente",
	t82: "Color de fuente",
	
	t255:"Traductor",
	t256:"Traducir a los chats entrantes: ",
	
	//Language Names: must match language_codes in the current style
	t257: ["apagar traductor", "�rabe", "b�lgaro", "catal�n", "chino simplificado", "chino tradicional", "checo", "dan�s", "holand�s", "ingl�s", "estonio", "finland�s", "franc�s", "alem�n", "griego", "criollo haitiano", "hebreo", "h�ngaro", "indonesio", "italiano", "japonese", "coreano", "let�n", "lituano", "noruego", "polaco", "portugu�s", "rumano", "ruso", "eslovaco", "esloveno", "espa�ol", "sueco", "tailand�s", "turco", "ucraniano", "vietnamita"],
	
	// Nombres de colores: deben coincidir color_values ??en el estilo actual
	t83: ["default", "negro", "azul", "rojo", "p�rpura", "verde", "amarillo", "anaranjaho", "blanco"],
	
	t84: "Lista de bloqueo",
	t85: "(A/V)",
	t86: "editar",
	t87: "eliminar",
	t88: "borrar",
	
	// Sala de di�logo de contrase�a
	t89: "Esta sala est� protegido con contrase�a.",
	t90: "Contrase�a",
	t91: "Entrar",
	t92: "Cancelar",
	t93: "Contrase�as debe contener entre 3 y 15 caracteres. S�lo letras, n�meros y guiones est�n permitidos.",
	t94: "La contrase�a es incorrecta. Int�ntelo de nuevo, por favor.",
	t95: "�Perd�n! El n�mero m�ximo de salas ha sido alcanzado.",
	
	// Di�logo de alerta
	alert_dialog_width: 260,
	t96: "<font style='color:red;font-weight:700'>Conexi�n p�rdida...</font> <a href='javascript:fc_chat.reqRecon()'>Reconectar</a> <a href = 'javascript:fc_chat.closeChat()'>Cerrar</a> ",
	t97: "Desconectando ...",
	t98: "<font style='color:red;font-weight:700'>�Perd�n! La sala ha cerrado...</font><br><center><a href='javascript:fc_chat.reqRecon()'>Reconectar</a> <a href='javascript:fc_chat.closeChat()'>Cerrar</a></center>",
	t99: "Para entrar,<b> Login </ b> primera, por favor. <a href='javascript:fc_chat.closeChat()'>Cerrar</a>",
	
	// Widget Msgs
	t100: "<b>Conectando. Espere, por favor...</ b>",
	t101: "Ha cerrado la sesi�n",
	t102: "<b>&nbsp;Chat est� apagado.&nbsp;&nbsp;</b>",
	t103: "<b>Chat en pausa... <a href='javascript:fc_chat.unPause()'>Reanudar</a></b>",
	t104: "<font style='color:red;font-weight:700'>Conexi�n p�rdida...</font> <a href='javascript:fc_chat.reqRecon()'>Reconnectar</a>",
	t105: "Error de conexi�n. Intentando de nuevo. Intento",
	t106: "�Perd�n! No se pude conectar. Renunciando..",
	t254: "Nuevo mensaje ... Pase el cursor sobre para ampliar.",
	
	// Salas de Chat
	t107: "�Bienvenido! Tu nombre de usuario es",
	t108: "Para iniciar sesi�n, haga clic en el nombre de usuario en la barra lateral.",
	t109: "En este momento, est� en la sala: ",
	t110: "<a href='javascript:void' onClick='fc_chat.show_hide_options(333,425);return false'>Vea la lista de salas</a>",
	t111: "Ante",
	t112: "Anterior",
	t113: "Siguiente",
	t114: "SISTEMA",
	t115: "a",
	t116: "Archivo Pagina",
	t117: "Chat actual",
	t118: "Auto-borrar. Sobrepasado el l�mite de mensajes.",
	t119: "**Comentarios Borrados**",
	t120: "<b><font style='margin-left:10px;font-size:12pt;'>&nbsp;Chat cargando ... Espere, por favor...</font></b>",
	t121: "<b><font style='margin-left:10px;font-size:12pt;'>&nbsp;Pagina cargando... Espere, por favor...</font></b>",
	t122: "<b><font style='margin-left:10px;font-size:12pt;'>&nbsp;Sala cargando... Espere, por favor...</font></b>",
	
	// Tipos de cuenta
	t123: "mod",
	t124: "admin",
	
	// Estado del usuario
	t125: "escribiendo",
	t126: "inactivo",
	
	// Alertas de habitaciones
	t127: "ha salido de la sala.",
	t128: "ha entrado en la sala.",
	t129: "Tienes que ser miembro para entrar en esta sala.",
	
	// Enviar mens chat
	t130: "No se puede enviar este mensaje. El usuario no est� en l�nea.",
	t131: "El mensaje es demasiado grande (",
	t132: "caracteres max).",
	t133: "Introduzca un mensaje, por favor!",
	
	// Dividir el modo de pantalla
	t134: "Modo de pantalla dividida no est� permitido.",
	
	// Remover de la lista de bloqueo
	t135: "Eliminar",
	
	// Fechas y horarios
	
	// Antes de minutos (singular / plural)
	t136: "Hace ",
	t137: "Hace ",
	// Despu�s de unos minutos (singular / plural)
	t138: " minuto",
	t139: " minutos",
	// Antes de las horas (singular / plural)
	t140: "Hace ",
	t141: "Hace ",
	// Despu�s de las horas (singular / plural)
	t142: " horas",
	t143: " horas",
	// Antes de d�a (singular / plural)
	t144: "Hace ",
	t145: "Hace ",
	// Despu�s de d�as (singular / plural)
	t146: " d�as",
	t147: " d�as",
	t148: "ahora mismo",
	t149: "AM",
	t150: "PM",
	
	// Aplicaci�n de Windows
	t151: "Ventana de Chat",
	t152: "FCChat Video",
	t153: "FCChat Video",
	t154: "minimizar",
	t155: "maximizar",
	t156: "restaurar",
	t157: "cerrar",
	
	// Msgs Otros Estado
	t158: "Espere, por favor",
	t159: "<b>Conectando. Espere, por favor...</ b>",
	t160: "No est� listo... Usted puede usar el chat-box, que se encuentra en la barra lateral, para entrar en las salas de chat.",
	t161: "Perd�n... Ha ocurrido un error. Recargar.",
	
	//Cargar p�gina
	t162: "Compartir im�genes",
	t163: "El usuario no est� v�lido. Int�ntelo de nuevo, por favor.",
	t164: "Nombre de la imagen no puede contener [[ o ]]. Int�ntelo de nuevo, por favor.",
	t165: "Mal tipo de archivo. Int�ntelo de nuevo, por favor.",
	t166: "Usted ha excedido el l�mite de tama�o para archivos de im�genes. Int�ntelo de nuevo, por favor!.",
	t167: "El repositorio de archivos de imagen est� lleno. Int�ntelo de nuevo, por favor!.",
	t168: "Introduzca un archivo v�lido, por favor.",
	t169: "�Error de subida! Int�ntelo de nuevo.",
	t170: "No hay imagen seleccionada. Int�ntelo de nuevo, por favor.",
	t171: "Se ha cargado con �xito.",
	t172: "Paso 1:",
	t173: "Paso 2:",
	t174: "Con el fin de utilizar esta imagen en sus mensajes de chat, simplemente copie y pegue el siguiente...",
	t175: "...en la ventana de chat a continuaci�n.",
	t176: "Sube la imagen que desea utilizar en sus mensajes de chat.",
	t177: "Cargar im�gen",
	t178: "(jpg, gif, y png s�lo. El tama�o m�ximo:",
	t179: "Nota:",
	t180: "Usted puede incluir un m�ximo de tres im�genes en los mensajes de chat solo.",
	t181: "Volver",
	t182: "Finalizar",
	
	// Avatar p�gina
	t183: "Subir Avatar",
	t184: "Seleccione Avatar",
	t185: "Espere...",
	t186: "Ha subido un nuevo avatar!",
	t187: "La anchura y la altura de las im�genes puede ser mayor que",
	t188: "px y",
	t189: "px, respectivamente. Int�ntalo de nuevo, por favor.",
	t190: "Opci�n Uno:",
	t191: "Opci�n Dos:",
	t192: "Opci�n Tres:",
	t193: "Opci�n Cuatro:",
	t194: "Cargar un nuevo avatar. El ancho y alto m�ximo para los avatares es",
	t195: "px",
	t196: "Usa tu",
	t197: "avatar",
	t198: "Enviar",
	t199: "Enlace a su gravatar imagen.<br>(es decir, http://www.gravatar.com/avatar/1234.png)",
	t200: "Utiliza tu avatar foro actual",
	t201: "Utilizar mi avatar actual",
	t202: "Seleccione un avatar de la galer�a de abajo",
	t203: "Usted tiene",
	t204: "seleccionado, con �xito, un avatar de la galer�a!",
	t205: "decidi� utilizar su avatar foro actual!",
	t206: "decidi� utilizar su avatar Gravatar!",
	
	// Perfil pagina
	t207: "Perfil de Usuario",
	t208: "Cargando...",
	t209: "�Perd�n! No perfil disponible.",
	t210: "�Perd�n! Los perfiles no est�n disponibles para las cuentas de invitados.",
	t211: "Reg�strate para crear un perfil, por favor",
	t212: "Nombre",
	t213: "Edad",
	t214: "Hombre",
	t215: "Mujer",
	t216: "G�nero",
	t217: "Location",
	t218: "Acerca de m�",
	t219: "caracteres",
	t220: "Guardar Perfil",
	t221: "Guardando...",
	t222: "Guardado",
	t223: "Volver al chat",
	
	// Video
	t224: "Video Chat Cargando ... Este proceso puede tardar unos segundos.",
	t225: "�Perd�n! Usted no puede abrir el v�deo chat de esta sala. Para utilizar el v�deo chat, debe estar en una habitaci�n que tiene (a/v) al lado de �l.",
	t226: "Este usuario est� charlando en la sala privada: ",
	t227: ". Usted debe ser un miembro de esta sala para ver su webcam.",
	t228: "�Perd�n! El v�deo chat ha alcanzado su capacidad. Int�ntelo m�s tarde.",
	t229: "Encienda la c�mara",
	t230: "Apague la c�mara",
	t231: "Audio",
	t232: "Silencio",
	t233: "Cargando",
	t234: "No Cam Encontrado",
	t235: "Iniciar la C�mara",
	t236: "Esperando la Se�al",
	t237: "Conexi�n P�rdida",
	t238: "Servidor Ocupado",
	t239: "Expansi�n",
	
	// Administraci�n
	t240: "censurar",
	// Antes del nombre
	t241: "Informe sobre",
	// Despu�s del nombre
	t242: ":",
	t243: "Este usuario ha sido bloqueado por el administrador del chat en todas las salas.",
	t244: "Este usuario ha bloqueado por el administrador del uso del chat privado.",
	t245: "Este usuario ha sido bloqueado el env�o de mensajes privados para aprox.",
	t246: "mins.",
	t247: "Este usuario ha sido bloqueado por el administrador del chat en esta sala.",
	t248: "Este usuario est� en buena posici�n.",
	t249: "La sala actual: ninguna.",
	t250: "La sala actual:",
	t251: "En L�nea:",
	t252: "El tiempo de inactividad: menos de un minuto.",
	t253: "El tiempo de inactividad:"

};